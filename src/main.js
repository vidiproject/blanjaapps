// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import Framework7 from 'framework7'
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle.js'
import Framework7Theme from 'framework7/dist/css/framework7.ios.min.css'

import MainStyle from './assets/sass/main.css'

import proximaFont from './assets/fonts/stylesheet.css'
import f7icon from './assets/css/framework7-icons.css'
import blanjaIcon from './assets/css/blanja_iconset.css'
import jStyle from './assets/css/gaya.css'
import customStyle from './assets/css/custom.css'

Vue.use(Framework7Vue, Framework7, Framework7Theme, MainStyle, proximaFont, f7icon, blanjaIcon, jStyle, customStyle, router)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
