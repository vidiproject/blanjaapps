import gql from 'graphql-tag';

export const tambahProduk = gql`
  mutation tambahProduct($input: ProductInput!) {
    createProduct(input: $input) {
      id
    }
  }
`;

export const tambahGambar = gql`
  mutation tambahGambar($id: ID!, $input: ImageInput!) {
    addProductImage(id: $id, input: $input)
  }
`;
